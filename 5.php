<?php
session_start();
include("sys/config.php");
$link = areas_db_connect();

$_SESSION['floor'] = '5';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Офіс - <?= $_SESSION['floor'] ?>-й поверх</title>
	<link rel="SHORTCUT ICON" href="/img/floor.ico" type="image/x-icon">
	<link href="css/layout_styles.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/floor_<?= $_SESSION['floor'] ?>.css" />
</head>

<body>

	<?php
	include('sys/pagination.php');
	?>

	<div style='margin-left: 0px'>

		<?php
		$min = $_SESSION['floor'] * 100;
		$max = $_SESSION['floor'] * 100 + 100;
		for ($k = $min; $k < $max; $k++) {
			$roomquan = 0;
			include('sys/query.php');
			include('sys/poster.php');
		}
		?>
		<div style="z-index: 1">
			<img src='img/<?= $_SESSION['floor'] ?>.jpg'>
		</div>
	</div>
</body>